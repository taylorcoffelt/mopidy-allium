import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.concurrent
import socket
from tornado.gen import coroutine
from tornado.process import Subprocess
from tornado.tcpclient import TCPClient
from concurrent import futures


class TCPStream:
    executor = futures.ThreadPoolExecutor(max_workers=1)
    s = None

    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect()

    def connect(self):
        print("Attempting to Connect")
        try:
            self.s.close()
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.connect(('localhost', 5000))
        except socket.error:
            pass



    @tornado.concurrent.run_on_executor
    def refresh(self):
        try:
            data = self.s.recv(1024)
            print("Done getting data")
            if not data:
                self.connect()
            else:
                BroadcastHandler.broadcast(data)
        except Exception:
            self.connect()
            pass
        tornado.ioloop.IOLoop.instance().add_callback(self.refresh)





@coroutine
def get_audio_data():
    # global stream
    yield stream.read_bytes(1024, BroadcastHandler.broadcast)
    # BroadcastHandler.broadcast("Hello")

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")


class BroadcastHandler(tornado.websocket.WebSocketHandler):
    clients = []

    def open(self):
        self.clients.append(self)

    def on_close(self):
        self.clients.remove(self)

    @classmethod
    def broadcast(cls, message):
        for client in cls.clients:
            client.write_message(message, True)


def make_app():
    # "Run" the future whenever we start the IOLoop and ignore the result
    # tornado.ioloop.IOLoop.instance().add_future(TCPStream.get_data(TCPStream()), lambda _: None)

    return tornado.web.Application([
        (r"/", MainHandler),
        (r"/websocket", BroadcastHandler)
    ])


if __name__ == "__main__":
    print("Starting Tornado Websocket on port 8888")
    app = make_app()
    # get_audio_data()
    ioloop = tornado.ioloop.IOLoop.instance()
    tcpstream = TCPStream()
    ioloop.add_callback(tcpstream.refresh)
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
