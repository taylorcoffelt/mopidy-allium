import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Mopidy from "mopidy";
import PCMPlayer from "./lib/PCMPlayer"


Vue.use(VueRouter);
Vue.use(Vuex);

const MopidyPort = 6680;

const store = new Vuex.Store({
    state: {
        mopidyConnectionState: 'disconnected',
        currentTrack: null,
        currentTrackPosition: null,
        playbackState: 'stopped',
        backgroundImage: null,
        colorPallete: null,
        trackList: []
    },
    mutations: {
        mopidyState (state, data) {
            state.mopidyConnectionState = data
        },
        currentTrack (state, data){
            state.currentTrack = data;
        },
        currentTrackPosition (state, data){
            state.currentTrackPosition = data;
        },
        trackList (state, data){
            state.trackList = data;
        },
        playbackState(state, data){
            state.playbackState = data;
        },
        setBackground(state, data){
            state.backgroundImage = data;
        },
        setColorPallete(state, data){
            state.colorPallete = data;
        }
    }
});

// Connect to Websocket Audio
var audioPlayer = new PCMPlayer({
    encoding: '16bitInt',
    channels: 2,
    sampleRate: 44100,
    flushingTime: 200
});
var exampleSocket = new WebSocket("ws://"+window.location.hostname+":"+MopidyPort+"/allium/audiows/");
exampleSocket.binaryType = 'arraybuffer';
exampleSocket.onmessage = function (event) {
    console.log(event);
    var data = new Int16Array(event.data);
    audioPlayer.feed(data);
};
exampleSocket.onopen = ()=>{
    console.log("Socket Connected");
};
exampleSocket.onclose = ()=>{
    console.log("Socket Closed");
};
exampleSocket.onerror = (e)=>{
    console.log("Error in Socket:",e);
};
window.exampleSocket = exampleSocket;

var mopidy = new Mopidy({
    webSocketUrl: "ws://"+window.location.hostname+":"+MopidyPort+"/mopidy/ws/",
    callingConvention: "by-position-or-by-name",
    autoConnect: false
});
window.mopidy = mopidy;
mopidy.on(console.log.bind(console));
mopidy.on("state:online", ()=>{
    store.commit("mopidyState", "connected");
    // var printCurrentTrack = function (track) {
    //     if (track) {
    //         console.log(track);
    //         // Get the image associated with this track
    //         console.log("Currently playing:", track.name, "by",
    //             track.artists[0].name, "from", track.album.name);
    //     } else {
    //         console.log("No current track");
    //     }
    // };
    mopidy.tracklist.setConsume([true]);
    mopidy.playback.getState()
        .done((data)=>{
            store.commit('playbackState', data);
        });
    mopidy.tracklist.getTracks().then((data)=>{
        store.commit("trackList",data);
    });
    mopidy.playback.getCurrentTrack()
        .done((track)=>{
            console.log(track);
            store.commit("setBackground", track ? track.album.images[0] : null);
            store.commit("currentTrack", track);
        });
});
mopidy.on("event:playbackStateChanged", (data)=>{
    store.commit('playbackState', data.new_state);
});
mopidy.on("event:trackPlaybackStarted", (data)=>{
    // Store the new background
    store.commit("setBackground",data.tl_track.track.album.images[0]);
    store.commit("currentTrack",data.tl_track.track);
});

mopidy.connect();

// setInterval(function(){
//     mopidy.playback.getTimePosition().then((data)=>{
//         store.commit("currentTrackPosition",data);
//     });
// }, 500);

mopidy.on("event:tracklistChanged", (data)=>{
    mopidy.tracklist.getTracks().then((data)=>{
        store.commit("trackList",data);
    });
});


Vue.config.productionTip = false;

const Foo = {
    template: '<div>{{connected}}</div>',
    computed: {
        connected () {
            return this.$store.state.mopidyConnectionState ? 'Connected' : 'Disconnected';
        }
    }
};
const Bar = { template: '<div>bar</div>' };
import Player from './pages/Player';
const routes = [
    { path: '/', component: Player },
    { path: '/foo', component: Foo },
    { path: '/bar', component: Bar }
];

const router = new VueRouter({
    routes // short for `routes: routes`
});

new Vue({
  render: h => h(App),
  store,
  router
}).$mount('#app');