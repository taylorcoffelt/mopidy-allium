from __future__ import unicode_literals

import logging
import os

from mopidy import config, ext
import mopidy
import tornado.escape
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.concurrent
import socket
from concurrent import futures
import json

__version__ = '0.0.2'

# TODO: If you need to log, use loggers named after the current Python module
logger = logging.getLogger(__name__)

votedSongs = {}

def websocket_factory(config, core):
    return [
        (r'/audiows/?', BroadcastHandler, {'core': core})
    ]


class TCPStream:
    executor = futures.ThreadPoolExecutor(max_workers=1)
    s = None

    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect()

    def connect(self):
        try:
            self.s.close()
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.connect(('localhost', 5000))
        except socket.error:
            pass

    @tornado.concurrent.run_on_executor
    def refresh(self):
        try:
            data = self.s.recv(1024)
            if not data:
                self.connect()
            else:
                BroadcastHandler.broadcast(data)
        except Exception:
            self.connect()
            pass
        tornado.ioloop.IOLoop.instance().add_callback(self.refresh)


class BroadcastHandler(tornado.websocket.WebSocketHandler):
    clients = []

    def initialize(self, core):
        self.core = core

    def open(self):
        self.clients.append(self)

    def on_close(self):
        self.clients.remove(self)

    def on_message(self, message):
        data = json.loads(message)
        if 'action' in data and data['action'] == "vote" and 'uri' in data and data['uri'] != '':
            self.core.tracklist.remove({'uri': [data['uri']]})
            # Add the Vote if allowed
            if data['uri'] in votedSongs:
                if not self.request.remote_ip in votedSongs[data['uri']]:
                    votedSongs[data['uri']].append(self.request.remote_ip)
                    # Remove the Song if necessary
                    if len(votedSongs[data['uri']]) > len(self.clients)/3:
                        self.core.tracklist.remove({'uri': [data['uri']]})
                    self.write_message(json.dumps({
                        'votedAs': self.request.remote_ip,
                        'numVotes': len(votedSongs[data['uri']])
                    }))
                else:
                    self.write_message(json.dumps({
                        'votedAs': self.request.remote_ip,
                        'duplicateVote': True
                    }))
            else:
                self.core.tracklist.remove(data['uri'])
                votedSongs[data['uri']] = [self.request.remote_ip]
                self.write_message(json.dumps({
                    'votedAs': self.request.remote_ip,
                    'numVotes': len(votedSongs[data['uri']])
                }))
        else:
            self.write_message("Access Denied")
        self.sendVotes()

    def sendVotes(self):
        self.broadcastMessage("Votes Updated")

    def check_origin(self, origin):
        return True

    @classmethod
    def broadcastMessage(cls, message):
        for client in cls.clients:
            client.write_message(message)

    @classmethod
    def broadcast(cls, message):
        for client in cls.clients:
            client.write_message(message, True)


class Extension(ext.Extension):

    dist_name = 'Mopidy-Allium'
    ext_name = 'allium'
    version = __version__

    def get_default_config(self):
        conf_file = os.path.join(os.path.dirname(__file__), 'ext.conf')
        return config.read(conf_file)

    def get_config_schema(self):
        schema = super(Extension, self).get_config_schema()
        # TODO: Comment in and edit, or remove entirely
        #schema['username'] = config.String()
        #schema['password'] = config.Secret()
        return schema

    def setup(self, registry):
        # You will typically only implement one of the following things
        # in a single extension.

        logger.debug("Starting Allium")

        tcpstream = TCPStream()
        ioloop = tornado.ioloop.IOLoop.instance()
        ioloop.add_callback(tcpstream.refresh)

        # TODO: Edit or remove entirely
        # from .frontend import FoobarFrontend
        # registry.add('frontend', FoobarFrontend)
        #
        # # TODO: Edit or remove entirely
        # from .backend import FoobarBackend
        # registry.add('backend', FoobarBackend)

        registry.add('http:static', {
            'name': self.ext_name,
            'path': os.path.join(os.path.dirname(__file__), 'static'),
        })
        registry.add('http:app', {
            'name': self.ext_name,
            'factory': websocket_factory
        })

