****************************
Mopidy-Allium
****************************

.. image:: https://img.shields.io/pypi/v/Mopidy-Allium.svg?style=flat
    :target: https://pypi.python.org/pypi/Mopidy-Allium/
    :alt: Latest PyPI version

.. image:: https://img.shields.io/travis/taylorcoffelt/mopidy-allium/master.svg?style=flat
    :target: https://travis-ci.org/taylorcoffelt/mopidy-allium
    :alt: Travis CI build status

.. image:: https://img.shields.io/coveralls/taylorcoffelt/mopidy-allium/master.svg?style=flat
   :target: https://coveralls.io/r/taylorcoffelt/mopidy-allium
   :alt: Test coverage

Mopidy extension for collaborative music


Installation
============

Install by running::

    pip install Mopidy-Allium

Or, if available, install the Debian/Ubuntu package from `apt.mopidy.com
<http://apt.mopidy.com/>`_.


Configuration
=============

Before starting Mopidy, you must add configuration for
Mopidy-Allium to your Mopidy configuration file::

    [allium]
    # TODO: Add example of extension config


Project resources
=================

- `Source code <https://gitlab.com/taylorcoffelt/mopidy-allium>`_
- `Issue tracker <https://gitlab.com/taylorcoffelt/mopidy-allium/issues>`_


Credits
=======

- Original author: `Taylor Coffelt <https://gitlab.com/taylorcoffelt`__
- Current maintainer: `Taylor Coffelt <https://gitlab.com/taylorcoffelt`__
- `Contributors <https://gitlab.com/taylorcoffelt/mopidy-allium/graphs/master>`_


Changelog
=========

v0.0.1 (UNRELEASED)
----------------------------------------

- Initial release.
