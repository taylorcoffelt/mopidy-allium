const path = require("path");

module.exports = {
    outputDir: path.resolve(__dirname, "../static"),
    runtimeCompiler: true,
    baseUrl: '/allium/'
};